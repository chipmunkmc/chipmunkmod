package land.chipmunk.chipmunkmod;

import land.chipmunk.chipmunkmod.data.BlockArea;
import net.minecraft.util.math.BlockPos;

public class Configuration {
  public CommandManager commands = new CommandManager();
  public CommandCore core = new CommandCore();
  public Bots bots = new Bots();

  public static class CommandManager {
    public String prefix = ".";
  }

  public static class CommandCore {
    public boolean enabled = true;
    public BlockArea relativeArea = new BlockArea(new BlockPos(0, 0, 0), new BlockPos(15, 0, 15));
    public land.chipmunk.chipmunkmod.modules.CommandCore.CommandSetMode commandSet = land.chipmunk.chipmunkmod.modules.CommandCore.CommandSetMode.ACTIONBAR;
  }

  public static class Bots {
    public BotInfo hbot = new BotInfo("#", null);
    public BotInfo sbot = new BotInfo(":", null);
    public BotInfo chipmunk = new BotInfo("'", null);
    public BotInfo chomens = new BotInfo("*", null);
    public BotInfo kittycorp = new BotInfo("^", null);
  }

  public static record BotInfo(String prefix, String key) {}
}
