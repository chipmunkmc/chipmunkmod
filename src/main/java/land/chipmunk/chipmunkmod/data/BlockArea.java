package land.chipmunk.chipmunkmod.data;

import net.minecraft.util.math.BlockPos;

// ? Am I reinventing the wheel here?
public record BlockArea(BlockPos start, BlockPos end) {}
