package land.chipmunk.chipmunkmod.modules;

import net.minecraft.client.MinecraftClient;
import net.minecraft.network.ClientConnection;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.network.packet.c2s.play.UpdateCommandBlockC2SPacket;
import net.minecraft.block.entity.CommandBlockBlockEntity;
import net.minecraft.nbt.NbtCompound;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.data.BlockArea;

public class CommandCore {
  private MinecraftClient client;
  public boolean ready = false; 
  public BlockPos origin;
  private final BlockArea relativeArea;
  public BlockPos currentBlockRelative;
  public boolean enabled;
  public CommandSetMode commandSetMode;

  public static CommandCore INSTANCE = new CommandCore(MinecraftClient.getInstance(), ChipmunkMod.CONFIG.core.relativeArea, ChipmunkMod.CONFIG.core.enabled, ChipmunkMod.CONFIG.core.commandSet);

  public CommandCore (MinecraftClient client, BlockArea relativeArea, boolean enabled, CommandSetMode commandSetMode) {
    this.client = client;
    this.relativeArea = relativeArea;
    this.enabled = enabled;
    this.commandSetMode = commandSetMode;
  }

  public void move (Vec3d position) {
    if (!enabled) return;

    if (!ready) {
      ready = true;
      // for (Listener listener : listeners) listener.ready();
    }

    origin = new BlockPos(
      ((int) position.getX() / 16) * 16,
      0, // TODO: Use the actual bottom of the world instead of hardcoding to 0
      ((int) position.getZ() / 16) * 16
    );

    if (currentBlockRelative == null) currentBlockRelative = new BlockPos(relativeArea.start());
    refill();
  }

  public void refill () {
    if (!enabled) return;

    final BlockPos relStart = relativeArea.start();
    final BlockPos relEnd = relativeArea.end();

    final String command = String.format(
      "fill %s %s %s %s %s %s command_block",
      relStart.getX() + origin.getX(),
      relStart.getY() + origin.getY(),
      relStart.getZ() + origin.getZ(),

      relEnd.getX() + origin.getX(),
      relEnd.getY() + origin.getY(),
      relEnd.getZ() + origin.getZ()
    );

    client.getNetworkHandler().sendChatCommand(command);
  }

  public void incrementCurrentBlock () {
    final BlockPos start = relativeArea.start();
    final BlockPos end = relativeArea.end();

    int x = currentBlockRelative.getX();
    int y = currentBlockRelative.getY();
    int z = currentBlockRelative.getZ();

    x++;

    if (x > end.getX()) {
      x = start.getX();
      z++;
    }

    if (z > end.getZ()) {
      z = start.getZ();
      y++;
    }

    if (y > end.getY()) {
      x = start.getX();
      y = start.getY();
      z = start.getZ();
    }

    currentBlockRelative = new BlockPos(x, y, z);
  }

  public BlockPos currentBlockAbsolute () {
    return currentBlockRelative.add(origin);
  }

  public void run (String command) {
    if (!enabled) return;

    final ClientConnection connection = client.getNetworkHandler().getConnection();
    final BlockPos currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    connection.send(new UpdateCommandBlockC2SPacket(currentBlock, "", CommandBlockBlockEntity.Type.REDSTONE, false, false, false));
    connection.send(new UpdateCommandBlockC2SPacket(currentBlock, command, CommandBlockBlockEntity.Type.REDSTONE, false, false, true));

    incrementCurrentBlock();
  }

  public CompletableFuture<NbtCompound> runTracked (String command) {
    if (!enabled) return emptyCompoundTagFuture();

    final ClientConnection connection = client.getNetworkHandler().getConnection();
    final BlockPos currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    connection.send(new UpdateCommandBlockC2SPacket(currentBlock, "", CommandBlockBlockEntity.Type.SEQUENCE, false, false, false));
    connection.send(new UpdateCommandBlockC2SPacket(currentBlock, command, CommandBlockBlockEntity.Type.REDSTONE, true, false, true));

    incrementCurrentBlock();

    CompletableFuture<NbtCompound> future = new CompletableFuture<NbtCompound>();

    final Timer timer = new Timer();

    final TimerTask queryTask = new TimerTask() {
      public void run () {
        client.getNetworkHandler().getDataQueryHandler().queryBlockNbt(currentBlock,
          tag -> { future.complete(tag); });

        timer.cancel(); // ? Is this necesary?
        timer.purge();
      }
    };

    timer.schedule(queryTask, 50);

    return future;
  }

  private CompletableFuture<NbtCompound> emptyCompoundTagFuture () {
    CompletableFuture<NbtCompound> future = new CompletableFuture<NbtCompound>();
    future.complete(new NbtCompound());
    return future;
  }

  public void cleanup () {
    origin = null;
    currentBlockRelative = null;
    ready = false;
  }

  public BlockArea getRelativeArea() {
    return this.relativeArea;
  }

  public static enum CommandSetMode {
    @SerializedName("shown") SHOWN,
    @SerializedName("hidden") HIDDEN,
    @SerializedName("actionbar") ACTIONBAR
  }
}
