package land.chipmunk.chipmunkmod.modules;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import com.mojang.brigadier.tree.CommandNode;
import com.mojang.brigadier.tree.LiteralCommandNode;
import java.util.Timer;
import java.util.TimerTask;

public class SelfCare {
  private final MinecraftClient client;
  private long interval;

  private Timer timer = null;

  public static final SelfCare INSTANCE = new SelfCare(MinecraftClient.getInstance(), 70L);

  public SelfCare (MinecraftClient client, long interval) {
    this.client = client;
    this.interval = interval;
  }

  public void init () {
    final TimerTask task = new TimerTask() {
      public void run () {
        tick();
      }
    };

    if (timer != null) cleanup();

    timer = new Timer();
    timer.schedule(task, interval, interval);
  }

  public void cleanup () {
    if (timer == null) return;

    timer.cancel();
    timer.purge();
    timer = null;
  }

  public void tick () {
    final ClientPlayerEntity player = client.player;
    final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

    if (networkHandler == null) {
      cleanup();
      return;
    }

    if (!player.hasPermissionLevel(2)) { if (serverHasCommand("op")) networkHandler.sendChatCommand("op @s[type=player]"); }
    else if (!client.player.isCreative()) networkHandler.sendChatCommand("gamemode creative");
  }

  // TODO: Move this into a separate class related to server info gathering (and yes, I plan on making this d y n a m i c and require little to no configuration for most servers)
  private boolean serverHasCommand (String name) {
    final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

    for (CommandNode node : networkHandler.getCommandDispatcher().getRoot().getChildren()) {
      if (!(node instanceof LiteralCommandNode)) continue;
      final LiteralCommandNode literal = (LiteralCommandNode) node;

      if (literal.getLiteral().equals(name)) return true;
    }

    return false;
  }

  public long getInterval() {
    return this.interval;
  }
}
