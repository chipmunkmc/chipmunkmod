package land.chipmunk.chipmunkmod;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.util.math.BlockPos;
import land.chipmunk.chipmunkmod.util.gson.*;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ChipmunkMod implements ModInitializer {
  // This logger is used to write text to the console and the log file.
  // It is considered best practice to use your mod id as the logger's name.
  // That way, it's clear which mod wrote info, warnings, and errors.
  public static final Logger LOGGER = LoggerFactory.getLogger("chipmunkmod");
  public static Configuration CONFIG;
  private static File CONFIG_DIR = new File("config");
  private static File CONFIG_FILE = new File(CONFIG_DIR, "chipmunkmod.json");

  @Override
  public void onInitialize () {
    // This code runs as soon as Minecraft is in a mod-load-ready state.
    // However, some things (like resources) may still be uninitialized.
    // Proceed with mild caution.

    try {
      CONFIG = loadConfig();
    } catch (IOException exception) {
      throw new RuntimeException("Could not load the config", exception);
    }

    LOGGER.info("Hello Fabric world!");
  }

  private static Configuration loadConfig () throws IOException {
    CONFIG_DIR.mkdirs();

    final Gson gson = new GsonBuilder()
      .registerTypeAdapter(BlockPos.class, new BlockPosTypeAdapter())
      .create();
    final File file = CONFIG_FILE;

    if (!file.exists()) {
      InputStream is = ChipmunkMod.class.getClassLoader().getResourceAsStream("assets/chipmunkmod/default_config.json");
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));

      final StringBuilder sb = new StringBuilder();
      while (reader.ready()) sb.append((char) reader.read());
      final String defaultConfig = sb.toString();

      // Write the default config
      BufferedWriter configWriter = new BufferedWriter(new FileWriter(file));
      configWriter.write(defaultConfig);
      configWriter.close();

      return gson.fromJson(defaultConfig, Configuration.class);
    }

    InputStream is = new FileInputStream(file);
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

    return gson.fromJson(reader, Configuration.class);
  }
}
