package land.chipmunk.chipmunkmod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import net.minecraft.text.Text;

@Mixin(net.minecraft.client.network.ClientPlayNetworkHandler.class)
public interface ClientPlayNetworkHandlerAccessor {
  @Accessor("CHAT_VALIDATION_FAILED_TEXT")
  public static Text chatValidationFailedText () { throw new AssertionError(); }
}
