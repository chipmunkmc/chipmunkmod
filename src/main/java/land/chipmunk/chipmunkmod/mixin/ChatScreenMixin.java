package land.chipmunk.chipmunkmod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.client.MinecraftClient;
import land.chipmunk.chipmunkmod.command.CommandManager;

@Mixin(net.minecraft.client.gui.screen.ChatScreen.class)
public class ChatScreenMixin {
  @Inject(at = @At("HEAD"), method = "sendMessage", cancellable = true)
  public void sendMessage(String chatText, boolean addToHistory, CallbackInfoReturnable<Boolean> cir) {
    final CommandManager commandManager = CommandManager.INSTANCE;

    if (chatText.startsWith(commandManager.prefix)) {
      commandManager.executeCommand(chatText.substring(commandManager.prefix.length()));

      if (addToHistory) MinecraftClient.getInstance().inGameHud.getChatHud().addToMessageHistory(chatText);

      cir.setReturnValue(true);
    }
  }
}
