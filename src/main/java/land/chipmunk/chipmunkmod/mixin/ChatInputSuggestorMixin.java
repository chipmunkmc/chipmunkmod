package land.chipmunk.chipmunkmod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import java.util.concurrent.CompletableFuture;
import com.mojang.brigadier.suggestion.Suggestions;
import net.minecraft.client.gui.widget.TextFieldWidget;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.client.MinecraftClient;
import land.chipmunk.chipmunkmod.command.CommandManager;

@Mixin(net.minecraft.client.gui.screen.ChatInputSuggestor.class)
public class ChatInputSuggestorMixin {
  @Shadow
  CompletableFuture<Suggestions> pendingSuggestions;

  @Shadow
  public boolean slashOptional;

  @Shadow
  public void show (boolean narrateFirstSuggestion) {}

  @Shadow
  final TextFieldWidget textField;

  public ChatInputSuggestorMixin () {
    textField = null;
  }

  @Inject(at = @At("TAIL"), method = "refresh()V")
  public void refresh (CallbackInfo ci) {
    if (slashOptional) return;

    final CommandManager commandManager = CommandManager.INSTANCE;

    final String text = this.textField.getText();
    final int cursor = this.textField.getCursor();

    if (cursor < commandManager.prefix.length() || !text.startsWith(commandManager.prefix)) return;

    final StringReader reader = new StringReader(text);
    reader.setCursor(commandManager.prefix.length()); // Skip the prefix

    final CommandDispatcher<FabricClientCommandSource> dispatcher = commandManager.dispatcher;
    final MinecraftClient client = MinecraftClient.getInstance();
    final FabricClientCommandSource commandSource = (FabricClientCommandSource) client.getNetworkHandler().getCommandSource();

    pendingSuggestions = dispatcher.getCompletionSuggestions(dispatcher.parse(reader, commandSource), cursor);
    show(true);
  }
}
