package land.chipmunk.chipmunkmod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.client.MinecraftClient;
import net.minecraft.network.packet.s2c.play.GameJoinS2CPacket;
import net.minecraft.network.packet.s2c.play.GameMessageS2CPacket;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.registry.CombinedDynamicRegistries;
import net.minecraft.client.network.ClientDynamicRegistryType;
import net.minecraft.text.Text;
import net.minecraft.text.TextContent;
import net.minecraft.text.TranslatableTextContent;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.command.CommandManager;
import land.chipmunk.chipmunkmod.modules.CommandCore;
import land.chipmunk.chipmunkmod.modules.SelfCare;

@Mixin(net.minecraft.client.network.ClientPlayNetworkHandler.class)
public class ClientPlayNetworkHandlerMixin {
  @Shadow private MinecraftClient client;
  @Shadow private FeatureSet enabledFeatures;
  @Shadow private CombinedDynamicRegistries<ClientDynamicRegistryType> combinedDynamicRegistries;

  @Inject(method = "onGameJoin", at = @At("TAIL"))
  private void onGameJoin (GameJoinS2CPacket packet, CallbackInfo ci) {
    final CommandRegistryAccess commandRegistryAccess = CommandRegistryAccess.of(this.combinedDynamicRegistries.getCombinedRegistryManager(), this.enabledFeatures);

    CommandManager.INSTANCE = new CommandManager(ChipmunkMod.CONFIG.commands.prefix, commandRegistryAccess);
    SelfCare.INSTANCE.init();
  }

  @Inject(method = "onGameMessage", at = @At("HEAD"), cancellable = true)
  private void onGameMessage (GameMessageS2CPacket packet, CallbackInfo ci) {
    final Text message = packet.content();

    final TextContent content = message.getContent();
    if (content instanceof TranslatableTextContent t_content && t_content.getKey().startsWith("advMode.setCommand")) {
      final CommandCore.CommandSetMode commandSetMode = CommandCore.INSTANCE.commandSetMode;

      if (commandSetMode == CommandCore.CommandSetMode.HIDDEN) {
        ci.cancel();
      } else if (commandSetMode == CommandCore.CommandSetMode.ACTIONBAR) {
        client.inGameHud.setOverlayMessage(message, false);
        ci.cancel();
      }

      return;
    }
  }
}
