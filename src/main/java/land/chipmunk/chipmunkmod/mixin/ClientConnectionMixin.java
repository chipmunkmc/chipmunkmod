package land.chipmunk.chipmunkmod.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.text.Text;
import land.chipmunk.chipmunkmod.modules.CommandCore;
import land.chipmunk.chipmunkmod.modules.SelfCare;

@Mixin(net.minecraft.network.ClientConnection.class)
public class ClientConnectionMixin {
  @Inject(at = @At("HEAD"), method = "disconnect", cancellable = true)
  public void disconnect (Text disconnectReason, CallbackInfo ci) {
    if (disconnectReason == ClientPlayNetworkHandlerAccessor.chatValidationFailedText()) {
      ci.cancel();
    }
  }
}
