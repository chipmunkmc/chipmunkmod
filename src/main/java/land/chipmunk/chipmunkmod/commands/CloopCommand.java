package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.LongArgumentType.longArg;
import static com.mojang.brigadier.arguments.LongArgumentType.getLong;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static com.mojang.brigadier.arguments.IntegerArgumentType.getInteger;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;
import land.chipmunk.chipmunkmod.modules.CommandLoopManager;
import java.util.List;

public class CloopCommand {
  private static final DynamicCommandExceptionType INVALID_CLOOP_ID_EXCEPTION = new DynamicCommandExceptionType(id -> Text.translatable("Invalid cloop id: %s", Text.literal(String.valueOf(id))));

  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("cloop")
        .then(
          literal("add")
            .then(
              argument("interval", longArg())
                .then(
                  argument("command", greedyString())
                    .executes(c -> addCloop(c))
                )
            )
        )
        .then(
          literal("remove")
            .then(
              argument("id", integer())
                .executes(c -> removeCloop(c))
            )
        )
        .then(
          literal("clear")
            .executes(c -> clearCloops(c))
        )
        .then(
          literal("list")
            .executes(c -> listCloops(c))
        )
    );
  }

  public static int addCloop (CommandContext<FabricClientCommandSource> context) {
    final FabricClientCommandSource source = context.getSource();
    final long interval = getLong(context, "interval");
    final String command = getString(context, "command");

    int id = CommandLoopManager.INSTANCE.loopCommand(command, interval);

    source.sendFeedback(Text.translatable("Successfully created a loop for command '%s' with id %s", Text.literal(command), Text.literal(String.valueOf(id))));
    return Command.SINGLE_SUCCESS;
  }

  public static int removeCloop (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final FabricClientCommandSource source = context.getSource();
    final CommandLoopManager manager = CommandLoopManager.INSTANCE;
    final int id = getInteger(context, "id");

    if (id < 0 || id >= manager.commandLoops.size()) throw INVALID_CLOOP_ID_EXCEPTION.create(id);

    manager.removeAndStop(id);

    source.sendFeedback(Text.translatable("Successfully removed loop with id %s", Text.literal(String.valueOf(id))));
    return Command.SINGLE_SUCCESS;
  }

  public static int clearCloops (CommandContext<FabricClientCommandSource> context) {
    final FabricClientCommandSource source = context.getSource();
    final CommandLoopManager manager = CommandLoopManager.INSTANCE;

    manager.clearLoops();

    source.sendFeedback(Text.translatable("Successfully cleared all command loops"));
    return Command.SINGLE_SUCCESS;
  }

  public static int listCloops (CommandContext<FabricClientCommandSource> context) {
    final FabricClientCommandSource source = context.getSource();
    final List<CommandLoopManager.CommandLoop> loops = CommandLoopManager.INSTANCE.commandLoops;

    int id = 0;
    for (CommandLoopManager.CommandLoop loop : loops) {
      source.sendFeedback(Text.translatable("%s: %s (%s)",  Text.literal(String.valueOf(id)), Text.literal(loop.command), Text.literal(String.valueOf(loop.getInterval()))));
      id++;
    }

    if (id == 0) {
      source.sendFeedback(Text.translatable("No command loops are currently running"));
    }

    return Command.SINGLE_SUCCESS;
  }
}
