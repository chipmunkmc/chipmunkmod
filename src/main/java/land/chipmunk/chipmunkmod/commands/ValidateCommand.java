package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.text.Text;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import java.util.Arrays;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.Configuration;
import land.chipmunk.chipmunkmod.util.Hexadecimal;

public class ValidateCommand {
  private static final SimpleCommandExceptionType UNSPECIFIED_KEY = new SimpleCommandExceptionType(Text.literal("The key of the bot is unspecified (null), did you incorrectly add it to your config?"));

  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("validate")
        .then(literal("hbot").then(argument("command", greedyString()).executes(c -> hbot(c))))
        .then(literal("sbot").then(argument("command", greedyString()).executes(c -> sbot(c))))
        // .then(literal("chipmunk").then(argument("command", greedyString()).executes(c -> chipmunk(c))))
        .then(literal("chomens").then(argument("command", greedyString()).executes(c -> chomens(c))))
        .then(literal("kittycorp").then(argument("command", greedyString()).executes(c -> kittycorp(c))))
    );
  }


  public static int hbot (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.hbot;
    final String command = getString(context, "command");
    final MinecraftClient client = context.getSource().getClient();
    final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

    final String prefix = info.prefix();
    final String key = info.key();
    if (key == null) throw UNSPECIFIED_KEY.create();

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      String time = String.valueOf(System.currentTimeMillis() / 10000);
      String input = command.replaceAll("&[0-9a-fklmnor]", "") + ";" + client.player.getUuidAsString() + ";" + time + ";" + key;
      byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
      BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
      String stringHash = bigInt.toString(Character.MAX_RADIX);

      networkHandler.sendChatMessage(prefix + command + " " + stringHash);
    } catch (NoSuchAlgorithmException e) {
      throw new SimpleCommandExceptionType(Text.literal(e.getMessage())).create();
    }

    return Command.SINGLE_SUCCESS;
  }

  public static int sbot (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.sbot;
    final String command = getString(context, "command");
    final MinecraftClient client = context.getSource().getClient();
    final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

    final String prefix = info.prefix();
    final String key = info.key();
    if (key == null) throw UNSPECIFIED_KEY.create();

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      String time = String.valueOf(System.currentTimeMillis() / 20000);
      String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + client.player.getName() + ";" + time + ";" + key;
      byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
      BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
      String stringHash = bigInt.toString(Character.MAX_RADIX);

      networkHandler.sendChatMessage(prefix + command + " " + stringHash);
    } catch (NoSuchAlgorithmException e) {
      throw new SimpleCommandExceptionType(Text.literal(e.getMessage())).create();
    }

    return Command.SINGLE_SUCCESS;
  }

  public static int chomens (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.chomens;
    final String command = getString(context, "command");
    final ClientPlayNetworkHandler networkHandler = context.getSource().getClient().getNetworkHandler();

    final String prefix = info.prefix();
    final String key = info.key();
    if (key == null) throw UNSPECIFIED_KEY.create();

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      String time = String.valueOf(System.currentTimeMillis() / 10000);
      String input = time + key;
      byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
      String stringHash = Hexadecimal.encode(hash).substring(0, 16);

      String[] arguments = command.split(" ");
      networkHandler.sendChatMessage(prefix + arguments[0] + " " + stringHash + " " + String.join(" ", Arrays.copyOfRange(arguments, 1, arguments.length)));
    } catch (NoSuchAlgorithmException e) {
      throw new SimpleCommandExceptionType(Text.literal(e.getMessage())).create();
    }

    return Command.SINGLE_SUCCESS;
  }

  public static int kittycorp (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.kittycorp;
    final String command = getString(context, "command");
    final ClientPlayNetworkHandler networkHandler = context.getSource().getClient().getNetworkHandler();

    final String prefix = info.prefix();
    final String key = info.key();
    if (key == null) throw UNSPECIFIED_KEY.create();

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      String time = String.valueOf(System.currentTimeMillis() / 10000);
      String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + time + ";" + key;
      byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
      BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
      String stringHash = bigInt.toString(Character.MAX_RADIX);

      networkHandler.sendChatMessage(prefix + command + " " + stringHash);
    } catch (NoSuchAlgorithmException e) {
      throw new SimpleCommandExceptionType(Text.literal(e.getMessage())).create();
    }

    return Command.SINGLE_SUCCESS;
  }
}
