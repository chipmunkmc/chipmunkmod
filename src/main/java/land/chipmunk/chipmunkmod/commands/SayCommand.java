package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

public class SayCommand {
  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("say")
        .then(
          argument("message", greedyString())
            .executes(SayCommand::say)
        )
    );
  }

  public static int say (CommandContext<FabricClientCommandSource> context) {
    final FabricClientCommandSource source = context.getSource();
    source.getClient().getNetworkHandler().sendChatMessage(getString(context, "message"));

    return Command.SINGLE_SUCCESS;
  }
}
